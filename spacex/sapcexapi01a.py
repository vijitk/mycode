import json
import urllib.request

SPACEXURI = "https://api.spacexdata.com/v3/cores"


def main():

    coredata =  urllib.request.urlopen(SPACEXURI)

    xstring = coredata.read().decode()
    print(type(xstring))

    listOfCores = json.loads(xstring)
    print(type(listOfCores))

    for core in listOfCores:
        print(core, end="\n\n")

if __name__ == "__main__":
    main()
